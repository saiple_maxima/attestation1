package com.innopolis.attestation;

import java.io.*;

public class Main {

    private static final File DATA_FILE = new File("/Users/saiple/Documents/Maxima/attestation1/resources/data.txt");
    private static final File ID_FILE = new File("/Users/saiple/Documents/Maxima/attestation1/resources/id.txt");
    private static final File TEMP_FILE = new File("/Users/saiple/Documents/Maxima/attestation1/resources/temp.txt");

    public static void main(String[] args) throws IOException {

        UsersRepositoryFile repository = new UsersRepositoryFileImpl(DATA_FILE, ID_FILE, TEMP_FILE);

        User user = repository.findById(2);

        User newUser = new User(6, "Ivan", "Ivanov", 31, true);
        repository.create(newUser);

        user.setName("Petr");
        repository.update(user);

//
        repository.delete(8);


        int i = 0;


    }

}
