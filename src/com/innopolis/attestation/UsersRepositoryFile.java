package com.innopolis.attestation;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface UsersRepositoryFile {
    User findById(int id) throws FileNotFoundException;
    void create(User user) throws IOException;
    void update(User user) throws IOException;
    void delete(int id) throws IOException;
}
